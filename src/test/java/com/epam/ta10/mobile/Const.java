package com.epam.ta10.mobile;

import com.epam.ta10.mobile.model.Message;

public class Const {

  private final static String MESSAGE_RECIPIENT = "pashkopanch@epam.com";
  private final static String MESSAGE_THEME = "Hello";
  private final static String MESSAGE_TEXT = "This is test message";
  public final static Message MESSAGE = new Message(MESSAGE_RECIPIENT, MESSAGE_THEME, MESSAGE_TEXT);

  private Const() {
  }
}
