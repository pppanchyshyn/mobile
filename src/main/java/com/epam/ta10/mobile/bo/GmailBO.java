package com.epam.ta10.mobile.bo;

import com.epam.ta10.mobile.model.Message;
import com.epam.ta10.mobile.po.GmailAccountChoicePage;
import com.epam.ta10.mobile.po.GmailCreateMessagePage;
import com.epam.ta10.mobile.po.GmailGreetingPage;
import com.epam.ta10.mobile.po.GmailPersonalPage;
import com.epam.ta10.mobile.po.GmailViewModeChoicePage;
import com.epam.ta10.mobile.po.GmailWhatNewInPage;
import com.epam.ta10.mobile.utils.WaiterProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GmailBO {

  private static Logger logger = LogManager.getLogger(GmailBO.class);
  private GmailWhatNewInPage gmailWhatNewInPage;
  private GmailAccountChoicePage gmailAccountChoicePage;
  private GmailGreetingPage gmailGreetingPage;
  private GmailViewModeChoicePage gmailViewModeChoicePage;
  private GmailPersonalPage gmailPersonalPage;
  private GmailCreateMessagePage gmailCreateMessagePage;

  public GmailBO() {
    gmailWhatNewInPage = new GmailWhatNewInPage();
    gmailAccountChoicePage = new GmailAccountChoicePage();
    gmailGreetingPage = new GmailGreetingPage();
    gmailViewModeChoicePage = new GmailViewModeChoicePage();
    gmailPersonalPage = new GmailPersonalPage();
    gmailCreateMessagePage = new GmailCreateMessagePage();
  }

  public GmailBO logInGmail() {
    logger.info("Moving to 'what's new in gmail' page. Pressing button 'got it'...");
    gmailWhatNewInPage.getGotItButton().click();
    logger.info("Account selection. Pressing button 'action done'...");
    WaiterProvider.waitElementAndClick(gmailAccountChoicePage.getActionDoneButton());
    logger.info("Moving to gmail greeting page. Pressing button 'dismiss'...");
    gmailGreetingPage.getDismissButton().click();
    logger.info("View mode selection. Pressing button 'dismiss'...");
    gmailViewModeChoicePage.getDismissButton().click();
    return this;
  }

  public GmailBO createMessage(Message message) {
    logger.info("Pressing button to create new message...");
    gmailPersonalPage.getComposeButton().click();
    logger.info("Entering message recipient...");
    gmailCreateMessagePage.getMessageRecipient().sendKeys(message.getRecipient());
    gmailCreateMessagePage.getMessageRecipient().submit();
    logger.info("Entering message theme...");
    gmailCreateMessagePage.getMessageSubject().sendKeys(message.getTheme());
    logger.info("Entering message text...");
    gmailCreateMessagePage.getMessageText().sendKeys(message.getText());
    return this;
  }

  public void sendMessage() {
    logger.info("Sending message...");
    gmailCreateMessagePage.getSendButton().click();
  }
}
