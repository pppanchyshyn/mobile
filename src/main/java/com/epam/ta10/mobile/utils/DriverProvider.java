package com.epam.ta10.mobile.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverProvider {

  private final static int TIMEOUT = 30;
  private static DesiredCapabilities cap = new DesiredCapabilities();
  private static WebDriver driver;


  static {
    cap.setCapability("deviceName", "MEIZU");
    cap.setCapability("udid", "88UFBMQ223ZL");
    cap.setCapability("platformName", "Android");
    cap.setCapability("platformVersion", "5.1");
    cap.setCapability("appPackage", "com.google.android.gm");
    cap.setCapability("appActivity", "com.google.android.gm.ConversationListActivityGmail");
  }

  private DriverProvider() {
  }

  public static WebDriver getDriver() {
    if (Objects.isNull(driver)) {
      try {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }
      driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
    }
    return driver;
  }

  public static void close() {
    if (!Objects.isNull(driver)) {
      driver.quit();
    }
  }
}
