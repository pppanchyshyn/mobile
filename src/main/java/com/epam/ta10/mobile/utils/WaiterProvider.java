package com.epam.ta10.mobile.utils;

import org.openqa.selenium.WebElement;

public class WaiterProvider {

  private WaiterProvider() {
  }

  public static void waitElementAndClick(WebElement webElement) {
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    webElement.click();
  }
}
