package com.epam.ta10.mobile.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPersonalPage extends AbstractPage {

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/compose_button']")
  private WebElement composeButton;

  @FindBy(xpath = "//*[@resource-id = '' and @class = 'android.widget.ImageButton']")
  private WebElement menuButton;

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/action_text']")
  private WebElement cancelButton;

  public WebElement getComposeButton() {
    return composeButton;
  }

  public WebElement getMenuButton() {
    return menuButton;
  }

  public WebElement getCancelButton() {
    return cancelButton;
  }
}
