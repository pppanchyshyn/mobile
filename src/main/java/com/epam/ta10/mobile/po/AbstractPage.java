package com.epam.ta10.mobile.po;

import com.epam.ta10.mobile.utils.DriverProvider;
import org.openqa.selenium.support.PageFactory;

abstract class AbstractPage {

  AbstractPage() {
    PageFactory.initElements(DriverProvider.getDriver(), this);
  }
}
