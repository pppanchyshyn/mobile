package com.epam.ta10.mobile.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailAccountChoicePage extends AbstractPage {

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/action_done']")
  private WebElement actionDoneButton;

  public WebElement getActionDoneButton() {
    return actionDoneButton;
  }
}
