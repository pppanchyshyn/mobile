package com.epam.ta10.mobile.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailCreateMessagePage extends AbstractPage {

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/to']")
  private WebElement messageRecipient;

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/subject']")
  private WebElement messageSubject;

  @FindBy(xpath = "//*[@resource-id = '' and @class = 'android.webkit.WebView']")
  private WebElement messageText;

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/send']")
  private WebElement sendButton;

  public WebElement getMessageRecipient() {
    return messageRecipient;
  }

  public WebElement getMessageSubject() {
    return messageSubject;
  }

  public WebElement getMessageText() {
    return messageText;
  }

  public WebElement getSendButton() {
    return sendButton;
  }
}
