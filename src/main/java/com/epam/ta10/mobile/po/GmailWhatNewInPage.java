package com.epam.ta10.mobile.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailWhatNewInPage extends AbstractPage{

  @FindBy(xpath ="//*[@resource-id = 'com.google.android.gm:id/welcome_tour_got_it']")
  private WebElement gotItButton;

  public WebElement getGotItButton() {
    return gotItButton;
  }
}