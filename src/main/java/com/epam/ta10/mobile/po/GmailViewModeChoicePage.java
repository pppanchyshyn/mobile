package com.epam.ta10.mobile.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailViewModeChoicePage extends AbstractPage {

  @FindBy(xpath = "//*[@resource-id = 'com.google.android.gm:id/gm_dismiss_button']")
  private WebElement dismissButton;

  public WebElement getDismissButton() {
    return dismissButton;
  }
}
